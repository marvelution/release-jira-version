#!/bin/bash
#
# Release to dockerhub.
#
# Required globals:
#   DOCKER_HUB_USERNAME
#   DOCKER_HUB_PASSWORD

set -ex

IMAGE=$1
VERSION=$(semversioner current-version)

echo ${DOCKER_HUB_PASSWORD} | docker login --username "$DOCKER_HUB_USERNAME" --password-stdin
docker build -t ${IMAGE} .
docker tag ${IMAGE} ${IMAGE}:${VERSION}
docker push ${IMAGE}
