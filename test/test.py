import json
import logging
import requests
import socket

from bitbucket_pipes_toolkit.test import PipeTestCase
from datetime import datetime

logger = logging.getLogger(__name__)


class ReleaseVersionTestCase(PipeTestCase):

    def get_ip(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            s.connect(('10.255.255.255', 1))
            ip = s.getsockname()[0]
        except:
            ip = '127.0.0.1'
        finally:
            s.close()
        return ip

    def wiremock_base_url(self):
        return f'http://{self.get_ip()}:8080'

    def wiremock_admin_base_url(self):
        return f'{self.wiremock_base_url()}/__admin'

    def wiremock_mappings_url(self):
        return self.wiremock_admin_base_url() + '/mappings'

    def tearDown(self):
        requests.delete(self.wiremock_mappings_url())
        requests.delete(self.wiremock_admin_base_url() + '/requests')

    def run_container(self, *args, **kwargs):
        jira_base_url = self.wiremock_base_url()

        logger.info(f'Using wiremock up on {jira_base_url}')

        kwargs['environment'].update({
            'JIRA_BASE_URL': jira_base_url,
            'JIRA_API_USER': 'admin',
            'JIRA_API_TOKEN': 'admin',
            'JIRA_PROJECT_KEY': 'BP'
        })
        return super().run_container(*args, **kwargs)

    def add_json_response(self, url, json, status=200, method='GET'):
        mapping = {
            'request': {
                'method': method,
                'url': url,
                'basicAuthCredentials': {
                    'username': 'admin',
                    'password': 'admin'
                }
            },
            'response': {
                'status': status,
                'headers': {
                    'Content-Type': 'application/json'
                },
                'body': json
            }
        }
        requests.post(
            self.wiremock_mappings_url(),
            headers={'Content-Type': 'application/json'},
            json=mapping
        )

    def add_json_file_response(self, url, json_file, status=200, method='GET'):
        json = {}
        with open(json_file, 'r') as file:
            json = file.read()
        self.add_json_response(url, json, status, method)

    def get_json_request(self, url, method='GET'):
        data = {
            'method': method,
            'url': url
        }
        response = requests.post(
            self.wiremock_admin_base_url() + '/requests/find',
            headers={'Content-Type': 'application/json'},
            json=data
        )
        return response.json()['requests']

    def test_no_successful_response(self):
        result = self.run_container(environment={
            'JIRA_VERSION': '0.1.0',
            'DEBUG': True
        })

        self.assertRegex(result, r'Failed to get the versions data.')
        self.assertRegex(result, r'Response \[404\]:')

    def test_no_versions_in_response(self):
        self.add_json_response('/rest/api/2/project/BP/versions', '[]')

        result = self.run_container(environment={
            'JIRA_VERSION': '0.1.0',
            'DEBUG': True
        })

        self.assertRegex(result, r'Failed to locate version 0.1.0 to release.')

    def test_non_existing_version(self):
        self.add_json_response('/rest/api/2/project/BP/versions', '[]')

        result = self.run_container(environment={
            'JIRA_VERSION': '1.1.1',
            'DEBUG': True
        })

        self.assertRegex(result, r'Failed to locate version 1.1.1 to release.')

    def test_non_existing_optional_version(self):
        self.add_json_response('/rest/api/2/project/BP/versions', '[]')

        result = self.run_container(environment={
            'JIRA_VERSION': '1.1.1',
            'OPTIONAL': True,
            'DEBUG': True
        })

        self.assertRegex(result, r'Unable to locate version 1.1.1 to release.')

    def test_release_version_not_found(self):
        self.add_json_file_response('/rest/api/2/project/BP/versions', 'test/versions.json')

        result = self.run_container(environment={
            'JIRA_VERSION': '0.1.0',
            'JIRA_VERSION_PREFIX': 'pipelines-',
            'DEBUG': True
        })

        self.assertRegex(result, r'Failed to locate version pipelines-0.1.0 to release.')

    def test_release_version(self):
        self.add_json_file_response('/rest/api/2/project/BP/versions', 'test/versions.json')
        self.add_json_response('/rest/api/2/version/10001', '{}', status=201, method='PUT')

        result = self.run_container(environment={
            'JIRA_VERSION': '0.1.0',
            'DEBUG': True
        })

        self.assertRegex(result, r'Pipe finished successfully marking version 0.1.0 as released.')

        requests = self.get_json_request('/rest/api/2/version/10001', 'PUT')
        self.assertEqual(len(requests), 1)
        request = requests[0]
        self.assertEqual(request['url'], '/rest/api/2/version/10001')
        self.assertEqual(request['method'], 'PUT')
        body = json.loads(request['body'])
        self.assertEqual(body['name'], '0.1.0')
        self.assertTrue(body['released'])
        self.assertEqual(body['releaseDate'], datetime.now().strftime('%Y-%m-%d'))

    def test_200_response_on_version_update(self):
        self.add_json_file_response('/rest/api/2/project/BP/versions', 'test/versions.json')
        self.add_json_response('/rest/api/2/version/10001', '{}', status=200, method='PUT')

        result = self.run_container(environment={
            'JIRA_VERSION': '0.1.0',
            'DEBUG': True
        })

        self.assertRegex(result, r'Pipe finished successfully marking version 0.1.0 as released.')

        requests = self.get_json_request('/rest/api/2/version/10001', 'PUT')
        self.assertEqual(len(requests), 1)
        request = requests[0]
        self.assertEqual(request['url'], '/rest/api/2/version/10001')
        self.assertEqual(request['method'], 'PUT')
        body = json.loads(request['body'])
        self.assertEqual(body['name'], '0.1.0')
        self.assertTrue(body['released'])
        self.assertEqual(body['releaseDate'], datetime.now().strftime('%Y-%m-%d'))

    def test_release_version_number(self):
        self.add_json_file_response('/rest/api/2/project/BP/versions', 'test/versions.json')
        self.add_json_response('/rest/api/2/version/10100', '{}', status=201, method='PUT')

        result = self.run_container(environment={
            'JIRA_VERSION': 1,
            'DEBUG': True
        })

        self.assertRegex(result, r'Pipe finished successfully marking version 1 as released.')

        requests = self.get_json_request('/rest/api/2/version/10100', 'PUT')
        self.assertEqual(len(requests), 1)
        request = requests[0]
        self.assertEqual(request['url'], '/rest/api/2/version/10100')
        self.assertEqual(request['method'], 'PUT')
        body = json.loads(request['body'])
        self.assertEqual(body['name'], '1')
        self.assertTrue(body['released'])
        self.assertEqual(body['releaseDate'], datetime.now().strftime('%Y-%m-%d'))

    def test_release_next_version(self):
        self.add_json_file_response('/rest/api/2/project/BP/versions', 'test/versions.json')
        self.add_json_response('/rest/api/2/version/10010', '{}', status=201, method='PUT')

        result = self.run_container(environment={
            'JIRA_VERSION': '1.0.0',
            'DEBUG': True
        })

        self.assertRegex(result, r'Pipe finished successfully marking version 1.0.0 as released.')
        requests = self.get_json_request('/rest/api/2/version/10010', 'PUT')
        self.assertEqual(len(requests), 1)
        request = requests[0]
        self.assertEqual(request['url'], '/rest/api/2/version/10010')
        self.assertEqual(request['method'], 'PUT')
        body = json.loads(request['body'])
        self.assertEqual(body['name'], '1.0.0')
        self.assertTrue(body['released'])
        self.assertEqual(body['releaseDate'], datetime.now().strftime('%Y-%m-%d'))

    def test_release_prefixed_version(self):
        self.add_json_file_response('/rest/api/2/project/BP/versions', 'test/prefixed-versions.json')
        self.add_json_response('/rest/api/2/version/10001', '{}', status=201, method='PUT')

        result = self.run_container(environment={
            'JIRA_VERSION': '0.1.0',
            'JIRA_VERSION_PREFIX': 'pipelines-',
            'DEBUG': True
        })

        self.assertRegex(result, r'Pipe finished successfully marking version pipelines-0.1.0 as released.')
        requests = self.get_json_request('/rest/api/2/version/10001', 'PUT')
        self.assertEqual(len(requests), 1)
        request = requests[0]
        self.assertEqual(request['url'], '/rest/api/2/version/10001')
        self.assertEqual(request['method'], 'PUT')
        body = json.loads(request['body'])
        self.assertEqual(body['name'], 'pipelines-0.1.0')
        self.assertTrue(body['released'])
        self.assertEqual(body['releaseDate'], datetime.now().strftime('%Y-%m-%d'))

    def test_release_next_prefixed_version(self):
        self.add_json_file_response('/rest/api/2/project/BP/versions', 'test/prefixed-versions.json')
        self.add_json_response('/rest/api/2/version/10010', '{}', status=201, method='PUT')

        result = self.run_container(environment={
            'JIRA_VERSION': '1.0.0',
            'JIRA_VERSION_PREFIX': 'pipelines-',
            'DEBUG': True
        })

        self.assertRegex(result, r'Pipe finished successfully marking version pipelines-1.0.0 as released.')
        requests = self.get_json_request('/rest/api/2/version/10010', 'PUT')
        self.assertEqual(len(requests), 1)
        request = requests[0]
        self.assertEqual(request['url'], '/rest/api/2/version/10010')
        self.assertEqual(request['method'], 'PUT')
        body = json.loads(request['body'])
        self.assertEqual(body['name'], 'pipelines-1.0.0')
        self.assertTrue(body['released'])
        self.assertEqual(body['releaseDate'], datetime.now().strftime('%Y-%m-%d'))

    def test_version_update_failed(self):
        self.add_json_file_response('/rest/api/2/project/BP/versions', 'test/prefixed-versions.json')
        self.add_json_response('/rest/api/2/version/10010', '{}', status=400, method='PUT')

        result = self.run_container(environment={
            'JIRA_VERSION': '1.0.0',
            'JIRA_VERSION_PREFIX': 'pipelines-',
            'DEBUG': True
        })

        self.assertRegex(result, r'Failed to update the version pipelines-1.0.0.')
        self.assertRegex(result, r'Response \[400\]:')
