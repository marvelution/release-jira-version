import requests
import yaml

from bitbucket_pipes_toolkit import Pipe, get_logger
from datetime import datetime
from requests.auth import HTTPBasicAuth

logger = get_logger()

schema = {
    'JIRA_BASE_URL': {
        'required': True,
        'type': 'string'
    },
    'JIRA_API_USER': {
        'required': True,
        'type': 'string'
    },
    'JIRA_API_TOKEN': {
        'required': True,
        'type': 'string'
    },
    'JIRA_PROJECT_KEY': {
        'required': True,
        'type': 'string'
    },
    'JIRA_VERSION': {
        'required': True
    },
    'JIRA_VERSION_PREFIX': {
        'type': 'string'
    },
    'OPTIONAL': {
        'default': False,
        'type': 'boolean'
    },
    'DEBUG': {
        'default': False,
        'type': 'boolean'
    }
}


class ReleaseVersion(Pipe):
    def _find_version(self, versions_data, version_name):
        for version_data in versions_data:
            if version_data['name'] == version_name:
                logger.info(f'Found version {version_name}')
                return version_data
        return None

    def run(self):
        super().run()

        base_url = self.get_variable('JIRA_BASE_URL')
        user = self.get_variable('JIRA_API_USER')
        token = self.get_variable('JIRA_API_TOKEN')

        project_key = self.get_variable('JIRA_PROJECT_KEY')
        version = self.get_variable('JIRA_VERSION')
        version_prefix = str(self.get_variable('JIRA_VERSION_PREFIX') or '')
        version_name = f'{version_prefix}{version}'

        headers = {'Content-Type': 'application/json'}
        auth = HTTPBasicAuth(user, token)

        response = requests.get(
            f'{base_url}/rest/api/2/project/{project_key}/versions',
            headers=headers,
            auth=auth
        )

        if not response.status_code == 200:
            self.fail(
                f'Failed to get the versions data.'
                f'\nResponse [{response.status_code}]: {response.text}'
            )

        versions_data = response.json()
        version_data = self._find_version(versions_data, version_name)

        if version_data is None:
            version_data = self._find_version(versions_data, f'{version_prefix}next')

        if version_data is None:
            if self.get_variable('OPTIONAL'):
                self.success(f'Unable to locate version {version_name} to release.', do_exit=True)
            else:
                self.fail(f'Failed to locate version {version_name} to release.')

        version_id = version_data['id']
        update_data = {
            'name': version_name,
            'released': True,
            'releaseDate': datetime.now().strftime('%Y-%m-%d')
        }

        response = requests.put(
            f'{base_url}/rest/api/2/version/{version_id}',
            headers=headers,
            auth=auth,
            json=update_data
        )

        if not response.ok:
            self.fail(
                f'Failed to update the version {version_name}.'
                f'\nResponse [{response.status_code}]: {response.text}'
            )

        self.success(f'Pipe finished successfully marking version {version_name} as released.')


if __name__ == '__main__':
    with open('/usr/bin/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = ReleaseVersion(
        pipe_metadata=metadata,
        schema=schema,
        check_for_newer_version=False
    )
    pipe.run()
