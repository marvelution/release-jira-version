# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.7

- patch: Corrected version update response check

## 0.0.6

- patch: Optional flag

## 0.0.5

- patch: Support non-string versions

## 0.0.4

- patch: Corrected versioning in pipe.yml

## 0.0.3

- patch: Improved local IP lookup

## 0.0.2

- patch: Formatting and cleanup

## 0.0.1

- patch: Initial Release

