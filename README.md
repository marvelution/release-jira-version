# Bitbucket Pipelines Pipe: release-jira-version

Mark a release in Jira as released, optionally looking for the `next` version to update and mark released.
**Works with Jira Server, Data Center and Cloud**

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: marvelution/release-jira-version:0.0.7
  variables:
    JIRA_BASE_URL: '<string>'
    JIRA_API_USER: '<string>'
    JIRA_API_TOKEN: '<string>'
    JIRA_PROJECT_KEY: '<string>'
    JIRA_VERSION: '<string>'
    JIRA_VERSION_PREFIX: '<string>' # Optional.
    OPTIONAL: <boolean> # Optional.
    DEBUG: <boolean> # Optional.
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| JIRA_BASE_URL (*) | URL of Jira instance. Example: `https://<yourdomain>.atlassian.net` or `https://issues.<yourdomain>.com` |
| JIRA_API_USER (*) | Username (Jira Server/Data Center) or Email address (Jira Cloud). Example: `human` or `human@example.com` |
| JIRA_API_TOKEN (*) | **Password**/**Access Token** for Authorization. Example: `HXe8DGg1iJd2AopzyxkFB7F2` ([Jira Cloud How To](https://confluence.atlassian.com/cloud/api-tokens-938839638.html)) |
| JIRA_PROJECT_KEY (*) | Key of the Jira project that contains the version that should be released. |
| JIRA_VERSION (*) | Name of the version to be released. In case the version doesn't exists it will try to update the `next` version. Example: `1.0.0` |
| JIRA_VERSION_PREFIX | Version name prefix. Example: `server-` |
| OPTIONAL | Flag whether the pipe should fail if the version to be released cannot be found. `False` by default. |
| DEBUG | Turn on extra debug information. `False` by default. |

_(*) = required variable._

## Details

This pipe attempts to mark a version of a specific Jira proejct as released, updating the `released` flag and the `releaseDate` date.
First it will attempt to locate the version `[JIRA_VERSION_PREFIX][JIRA_VERSION]`, eg. `pipeline-0.1.0`. In case this version doesn't
 exists for the specified Jira project (through `JIRA_PROJECT_KEY`), then the pipe will attempt to locate the `[JIRA_VERSION_PREFIX]next`
 version, eg. `pipeline-next`. If this `next` version is located, then it will be renamed to `[JIRA_VERSION_PREFIX][JIRA_VERSION]`, eg.
 `pipeline-0.1.0` next to updating the `released` flag and the `releaseDate` date.

## Prerequisites

To release a version in Jira Cloud, you need to create a Personal access token. You can follow the instructions
 [here](https://confluence.atlassian.com/cloud/api-tokens-938839638.html) to create one.

## Examples

Basic example:

```yaml
script:
  - pipe: marvelution/release-jira-version:0.0.7
    variables:
      JIRA_BASE_URL: $JIRA_BASE_URL
      JIRA_API_USER: $JIRA_API_USER_EMAIL
      JIRA_API_TOKEN: $JIRA_API_TOKEN
      JIRA_PROJECT_KEY: "BP"
      JIRA_VERSION: "0.1.0"
```

Example with version prefix:

```yaml
script:
  - pipe: marvelution/release-jira-version:0.0.7
    variables:
      JIRA_BASE_URL: $JIRA_BASE_URL
      JIRA_API_USER: $JIRA_API_USER_EMAIL
      JIRA_API_TOKEN: $JIRA_API_TOKEN
      JIRA_PROJECT_KEY: "BP"
      JIRA_VERSION: "0.1.0"
      JIRA_VERSION_PREFIX: "pipeline-"
```

## Support

If you'd like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License

Copyright (c) 2019 Marvelution.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,jira
